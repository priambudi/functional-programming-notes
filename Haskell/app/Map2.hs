module Map2 where

-- map akan membuat isi list xs menjadi 
-- Integer 1 semua, lalu sum akan menjumlahkan 
-- isi list xs yang dijadikan 1 tiap elemennya.
-- 
-- lex [1,2,3]
-- 3

-- What does map (+1) (map (+1) xs) do? Can you 
-- conclude anything in general about properties of 
-- map f (map g xs), where f and g are arbitrary 
-- functions?
-- 
-- map (+1) (map (+1) xs) will add 1 and then add another 
-- 1 to every element of xs. In other words, it will add 2
--  to each element of xs.
-- 