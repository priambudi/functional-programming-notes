module LazyEvaluation where

divisor n = [x | x <- [1..n], n `mod` x == 0]

quickSort [] = []
quickSort (x:xs) = quickSort [y | y<-xs, y<x] ++ [x] ++ quickSort [y | y<-xs, y>x]