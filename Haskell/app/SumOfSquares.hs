module SumOfSquares where

-- How would you define the sum of the squares
-- of the natural numbers 1 to n using map and foldr?
sumOfSquares xs = foldr (+) 0 (map (^2) xs)

sumOfSquaresLC xs = sum[z*z | z<-xs]