module Multiplication where

data Cond a = a :? a

infixl 0 ?
infixl 1 :?

(?) :: Bool -> Cond a -> a
True  ? (x :? _) = x
False ? (_ :? y) = y

posMul a b = b == 0 ? 0 :? a + posMul a (b-1)
