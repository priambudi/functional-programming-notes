module Robot where

-- import Array
-- import Data.List
-- import Data.Monad
-- import SOEGraphics
-- import Win32Misc(timeGetTime)
-- import qualified GraphicsWindows as GW (getEvent)

-- data RobotState 
--     = RobotState 
--         { position :: Position, 
--         facing :: Direction, 
--         pen :: Bool, 
--         color :: Color, 
--         treasure :: [Position], 
--         pocket :: Int
--         } deriving Show

-- type Position = (Int, Int)

-- data Direction = North | East | South | West 
--     deriving (Eq,Show,Enum)

-- type Robot1 a = RobotState -> Grid -> (RobotState, a)

-- type Robot2 a = RobotState -> Grid -> Window -> (RobotState, a, IO ())

-- type Robot3 a = RobotState -> Grid -> Window -> IO (RobotState, a)

-- newtype Robot a 
    -- = Robot (RobotState -> Grid -> Window -> IO (RobotState, a))

-- right :: Direction -> Direction
-- right d = toEnum (succ (fromEnum d) `mod` 4)

-- newtype Robot a = Robot (RobotState -> Grid -> Window -> IO (RobotState, a))

-- updateState u = Robot (\s __ -> return (u s, ()))