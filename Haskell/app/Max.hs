module Max where
maxOf :: Int -> Int -> Int -> Int
maxOf a b c = a `max` b `max` c

maxOf2 a b = if a > b then a else b
maxOf3 a b = maxOf2 (maxOf2 a b)

-- http://learnyouahaskell.com/recursion
maximum' :: (Ord a) => [a] -> a
maximum' [x] = x  
maximum' (x:xs) = max x (maximum' xs)  