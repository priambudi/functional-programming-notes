module Iter where

iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)
-- iter 3 f x = f (f (f x))