module Flip where

flipMe :: (a -> b -> c) -> (b -> a -> c)
flipMe f x y = f y x