{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}
module Currying where

reversee xs = rev revOp [] xs
    where rev op acc [] = acc
          rev op acc (x:xs) = rev op (acc `op` x) xs

-- reversee [1,2,3]
-- rev revOp [] [1,2,3]
-- rev revOp [1] [2,3]
-- rev revOp [2,1] [3]
-- rev revOp [3,2,1] []
-- [3,2,1]

reverseFoldl xs = foldl revOp [] xs
    where revOp acc x = x:acc

flipMe :: (a -> b -> c) -> (b -> a -> c)
flipMe f x y = f y x

revOp = flipMe (:)

newReverse = foldl (flipMe (:)) []

-- Exercise 9.2
-- Show that flip (flip f) is f
-- flip (flip f) a b = f a b
-- flip f b a        = f a b
-- f a b             = f a b

-- Exercise 9.4
applyEach = newApply where
    newApply [] a = []
    newApply (x:xs) a = x a : applyEach xs a

-- Exercise 9.5
-- applyAll xs = newApply (newReverse xs) where
--     newApply [] a = a
--     newApply (x:xs) a = newApply xs (x a)
applyAll xs = newApply (newReverse xs) where
    newApply xs a = foldl (\ a x -> x a) a xs

-- Exercise 9.7
twice :: (a -> a) -> a -> a
twice f = f . f

-- Exercise 9.8
power :: (a -> a) -> Integer -> a -> a
power f 1 = f
power f a = f . power f (a-1)