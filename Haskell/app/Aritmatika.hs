module Aritmatika where

data Expr = C Float 
        | Expr :+ Expr 
        | Expr :- Expr
        | Expr :* Expr 
        | Expr :/ Expr
        | V String
        | Let String Expr Expr
    deriving Show

add :: Expr -> Expr -> Expr    
add (C c) (C d) = C (c+d)

sub :: Expr -> Expr -> Expr    
sub (C c) (C d) = C (c-d)

mul :: Expr -> Expr -> Expr    
mul (C c) (C d) = C (c*d)

divide :: Expr -> Expr -> Expr    
divide (C c) (C d) = C (c/d)

foldArithmetic (a,s,m,d) (C c) = c
foldArithmetic (a,s,m,d) (e1 :+ e2 ) = a (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)
foldArithmetic (a,s,m,d) (e1 :- e2 ) = s (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)
foldArithmetic (a,s,m,d) (e1 :* e2 ) = m (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)
foldArithmetic (a,s,m,d) (e1 :/ e2 ) = d (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)

-- newEvaluate expr = foldArithmetic (aa, as, am, ad) expr
newEvaluate = foldArithmetic (aa, as, am, ad)
    where aa = (+)
          as = (-)
          am = (*)
          ad = (/)

evaluate :: Expr -> Float
evaluate (C x)          = x
evaluate (e1 :+ e2)     = evaluate e1 + evaluate e2
evaluate (e1 :- e2)     = evaluate e1 - evaluate e2
evaluate (e1 :* e2)     = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)     = evaluate e1 / evaluate e2
evaluate (Let v e0 e1)  = evaluate (subst v e0 e1)
evaluate (V _)          = 0.0 -- is this correct?

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)             = if v0 == v1 then e0 else V v1
subst _ _ (C c)                = C c
subst v0 e0 (e1:+e2)           = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1:-e2)           = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1:*e2)           = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1:/e2)           = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2)     = Let v1 e1 (subst v0 e0 e2) -- perlu koreksi ???

-- evaluate (Let "a" (C 2) (C 3))
-- 3.0

-- evaluate (Let "a" (C 2) ((V "a") :+ (C 2)))
-- 4.0