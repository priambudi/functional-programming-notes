module ListComprehension where

one xs = [ x+1 | x <- xs ]
oneLet xs = [ y | x <- xs, let y = x + 1 ]

oneHof = map (+1)

two xs ys = [ x+y | x <- xs, y <-ys ]
twoHof xs ys = concat(map (\x -> map(\y -> x+y)ys)xs )

three xs = [ x+2 | x <- xs, x > 3 ]
threeHof xs = map (+2) (filter (>3) xs)

four xys = [ x+3 | (x,_) <- xys ]
fourHof xys = map (\(x,y) -> x+3) xys

five xys = [ x+4 | (x,y) <- xys, x+y < 5 ]
fiveHof xys = map (\(x,y) -> x+4) (filter (\(x,y) -> x+y<5) xys)

lc1 xs = [ x+3 | x <- xs]
lc2 xs = [ x | x <- xs, x > 7]
lc3 xs ys = [ (x,y) | x <- xs, y <- ys ]
lc4 xys = [ x+y | (x,y) <- xys, x+y > 3]