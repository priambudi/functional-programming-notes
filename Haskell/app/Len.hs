module Len where

-- Define the length function using map and sum.
len xs = sum(map (const 1) xs)

lenFoldr xs = foldr (+) 0 (map (const 1) xs)
lenFoldrSimple xs = foldr ((+) . const 1) 0 xs

lenRecursive [] = 0
lenRecursive (x:xs) = 1 + lenRecursive xs

-- https://bigonotation.altervista.org/2017/04/24/how-to-find-length-list-haskell/
lenListComprehension :: (Num b) => [a] -> b
lenListComprehension [] = 0
lenListComprehension xs = sum[1|_<-xs]

-- Here the code creates a list of 1 by using
-- List Comprehension, and then sums it.