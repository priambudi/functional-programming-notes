module Main where

main :: IO ()

-- 1
solveMeFirst a b = a + b
main = do
    val1 <- readLn
    val2 <- readLn
    let sum = solveMeFirst val1 val2
    print sum

greet name = "Hello " ++ name ++ "!"

divisor n = [x | x<-[1..n],n`mod`x == 0]

pythaTriple = [(x,y,z) | z<-[5..], y<-[4..z], x<-[3..y], x^2+y^2==z^2]

hello 1 = putStrLn "Hello World"
hello n = do
    putStrLn "Hello World"
    hello (n-1)

fib 1 = 0
fib 2 = 1
fib n = fib (n-1) + fib (n-2)

len xs = sum (map (const 1) xs)

mystery xs = map (+1) (map (+1) xs)
mystery2 = map ((+1).(+1))

sum2 xs = foldr (+) 0 xs

iter 1 f x = f x
iter n f x = do
    f x
    iter (n - 1) f x

a xs = foldr (+) 0 (map (^2) xs)

hehe xs ys = concat (map (\x -> map (x+) ys) xs)
hehe2 xs ys = map (\x -> map (\y -> (x,y)) ys) xs

hehe3 (xs:x) = x
b xys = [ x+y | (x,y)<-xys, x+y>3  ]

sumOdd :: [Integer] -> [Integer]
sumOdd xs = [if y`mod`2 == 0 then y else y*y|y<-xs]

maxOf x y z = x `max` y `max` z

-- lab [] = []
lab x = 1
lax xs = [2|y<-xs, len y == 2]

listsum [] = 0
listsum (x:xs) = x + listsum xs
aa = listsum [1,2,3,4]