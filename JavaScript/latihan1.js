// 1
const isDefined = x => typeof x !== "undefined";

function head([x]) {
  return x;
}

function tail([, ...xs]) {
  return xs;
}

const map = (fn, [x, ...xs], i) => {
  i = i || 0;
  return isDefined(x) ? [fn(x, i), ...map(fn, xs, i + 1)] : [];
};

const reduce = (f, [x, ...xs], total = 0, i = 0) => {
  total = 0 || total;
  return isDefined(x) ? reduce(f, xs, f(total, x, i), i + 1) : total;
};

const plus = (total, num) => total + num;

function sum(array) {
  return reduce(plus, array);
}

function length(array) {
  return sum(map(() => 1, array));
}

console.log(length([1, 2, 3, 4, 5]));
// 5

console.log(map(x => x + 1, [1, 2, 3, 4, 5]));
// [2, 3, 4, 5, 6]

console.log(map(x => x + 1, map(x => x + 1, [1, 2, 3, 4, 5])));
// [3, 4, 5, 6, 7]

// 2
// # map (+1) (map (+1) xs) will add 2 to xs
// # map f (map g xs) will do map g xs first, then with the result,
//   do map f result

// 4
// # \n -> iter n succ is an anonymous function.
const succ = num => num + 1;

const iter = (n, f, x) => (n <= 0 ? x : iter(n - 1, f, f(x)));

console.log(iter(8, succ, 0));
// 8

// 5
function sumOfSquareOf(array) {
  return reduce(plus, map(x => x * x, array));
}

console.log(sumOfSquareOf([1, 2, 3]));

const reverse = ([x, ...xs]) => (isDefined(x) ? [...reverse(xs), x] : []);

const reverseReduce = xs => reduce((rest, x) => [x, ...rest], xs, []);

const flow = (...args) => init => reduce((memo, fn) => fn(memo), args, init);

function compose(...args) {
  return flow(...reverse(args));
}

const flip = (a, b, c) => b(a, c);

const someFlow = flow(
  x => x * x,
  x => x + 3
);
console.log(
  map(
    flow(
      someFlow,
      x => x * 0.5
    ),
    [5, 10, 20]
  )
);

console.log(
  map(
    compose(
      someFlow,
      x => x * 0.5
    ),
    [5, 10, 20]
  )
);

console.log(flip(3, (x, y) => x / y, 100));

const filter = ([x, ...xs], fn) =>
  isDefined(x) ? (fn(x) ? [x, ...filter(xs, fn)] : [...filter(xs, fn)]) : [];

const numberOne = xs => map(x => x + 1, xs);
console.log(numberOne([1, 2, 3, 4]));

const numberTwo = (xs, ys) => map((x, index) => x + ys[index], xs);
console.log(numberTwo([1, 2], [2, 3]));

const numberThree = xs => map(x => x + 2, filter(xs, x => x > 3));
console.log(numberThree([1, 2, 3, 4, 5, 6, 7]));

// quicksort
// quickSort [] = []
// quickSort [x:xs] = [y | y <- xs, y <= x] ++ [x] ++ [y | y <- xs, y >= x]

const quickSort = array =>
  length(array) === 0
    ? []
    : quickSort(filter(tail(array), y => y < head(array)))
        .concat(head(array))
        .concat(quickSort(filter(tail(array), y => y > head(array))));

for (var a = [], i = 0; i < 5000; ++i) a[i] = i;
// http://stackoverflow.com/questions/962802#962890
function shuffle(array) {
  var tmp,
    current,
    top = array.length;
  if (top)
    while (--top) {
      current = Math.floor(Math.random() * (top + 1));
      tmp = array[current];
      array[current] = array[top];
      array[top] = tmp;
    }
  return array;
}

a = shuffle(a);

// var start = new Date().getTime();
// console.log(quickSort(a));
// var end = new Date().getTime();

// var start2 = new Date().getTime();
// console.log(a.sort());
// var end2 = new Date().getTime();

// var time = end - start;
// var time2 = end2 - start2;
// console.log('FP Execution time: ' + time);
// console.log('OOP Execution time: ' + time2);
// [ x+3 | x <- xs ]
// [ x | x <- xs, x > 7 ]
// [ (x,y) | x <- xs, y <- ys ]

// 2 3 4 5 6 7 8 9
// 2  3 4 5 6 7 8 9
// 2  3 5 7 9
// 2 3  5 7 9
// 2 3  5 7
// 2 3 5  7
// 2 3 5  7
// 2 3 5 7
const isArray = x => Array.isArray(x);

const flatten = ([x, ...xs]) =>
  isDefined(x)
    ? isArray(x)
      ? [...flatten(x), ...flatten(xs)]
      : [x, ...flatten(xs)]
    : [];

const concat = (a, b) => {
  return flatten([a, b]);
};

const listWithRange = (start, end) =>
  start === end ? [start] : [start, ...listWithRange(start + 1, end)];

const sieve = n => getPrimeWithSieve(listWithRange(2, n));

const getPrimeWithSieve = ([x, ...xs]) =>
  isDefined(x)
    ? concat([x], getPrimeWithSieve(filter(xs, y => y % x !== 0)))
    : [];

console.log(sieve(10));
// [2, 3, 5, 7]
