function randLetter() {
  var letters = ["k", "p", "n"];
  var letter = letters[Math.floor(Math.random() * letters.length)];
  return letter;
}

function shuffle(array) {
  var tmp,
    current,
    top = array.length;
  if (top)
    while (--top) {
      current = Math.floor(Math.random() * (top + 1));
      tmp = array[current];
      array[current] = array[top];
      array[top] = tmp;
    }
  return array;
}

let generateList = n => {
  let pp = [];
  for (var i = 0; i < Math.floor(n / 2) + 1; i++) {
    pp.push("k");
  }
  pp.push(randLetter());
  for (var j = Math.floor(n / 2); j < n; j++) {
    pp.push(randLetter());
  }
  return pp;
};

let ask = (asker, answerer) => {
  if (asker === "k") {
    return answerer;
  } else if (asker === "p") {
    if (answerer === "k") {
      if (Math.round(Math.random() * 10) % 2 === 0) {
        return "p";
      } else return "n";
    } else if (answerer === "n") {
      if (Math.round(Math.random() * 10) % 2 === 0) {
        return "p";
      } else return "k";
    } else {
      if (Math.round(Math.random() * 10) % 2 === 0) {
        return "k";
      } else return "n";
    }
  } else {
    if (Math.round(Math.random() * 10) % 3 === 0) {
      return "k";
    } else if (Math.round(Math.random() * 10) % 2 === 0) {
      return "p";
    } else return "n";
  }
};

function mode(array) {
  if (array.length == 0) return null;
  var modeMap = {};
  var maxEl = array[0],
    maxCount = 1;
  for (var i = 0; i < array.length; i++) {
    var el = array[i];
    if (modeMap[el] == null) modeMap[el] = 1;
    else modeMap[el]++;
    if (modeMap[el] > maxCount) {
      maxEl = el;
      maxCount = modeMap[el];
    }
  }
  return maxEl;
}

let jeff = p => {
  let tempArray = [];
  let guessedArray = [];

  for (var i = 0; i < p.length; i++) {
    // if (i % 10 === 0) {
    //   console.log(i);
    // }
    for (var j = 0; j < p.length; j++) {
      if (i === j) {
        continue;
      }
      tempArray.push(ask(p[j], p[i]));
    }
    console.log(
      "Penduduk ke " + (i + 1) + " adalah seorang " + mode(tempArray)
    );
    if (mode(tempArray) === "k") {
      for (var k = i; k < p.length; k++) {
        guessedArray.push(ask("k", p[k]));
      }
      return guessedArray;
    }
    guessedArray.push(mode(tempArray));
    tempArray = [];
  }
  return guessedArray;
};

let smullyanIsland = shuffle(generateList(100000))
var start = new Date().getTime();
console.log(
  JSON.stringify(jeff(smullyanIsland)) === JSON.stringify(smullyanIsland)
);
var end = new Date().getTime();
console.log(end-start)
